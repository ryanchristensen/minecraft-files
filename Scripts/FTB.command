#! /bin/sh

cd /Users/zeus/Minecraft\ Servers/FTB_Server/

java -server -XX:UseSSE=4 -XX:+UseCMSCompactAtFullCollection -XX:ParallelGCThreads=4 -XX:+UseConcMarkSweepGC -XX:+DisableExplicitGC -XX:+CMSIncrementalMode -XX:+CMSIncrementalPacing -XX:+UseCompressedOops -XX:+AggressiveOpts -Xms6G -Xmx6G -jar "ftbserver.jar" nogui